import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *  # noqa

DEBUG = False
TEMPLATE_DEBUG = False

sentry_sdk.init(
    dsn=os.getenv('BOILERPLATE_SENTRY_DSN'),
    integrations=[DjangoIntegration()],
    send_default_pii=True,
)

SECRET_KEY = os.getenv('SECRET_KEY', SECRET_KEY)

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
