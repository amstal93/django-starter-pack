"""
App specific configurations.
"""

SPECTACULAR_DEFAULTS = {
    'TITLE': 'Boilerplate',
    'DESCRIPTION': 'Boilerplate API',
    'SERVERS': [],
}
